package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckPasswordLength() {
		boolean password = PasswordValidator.checkPasswordLength("12345678911007");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);
	}
	@Test 
	public void testCheckPasswordLengthExcpetion() throws Exception {
		boolean password = PasswordValidator.checkPasswordLength("1234");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);
		}
		
		
	
	@Test 
	public void testCheckPasswordLengthIn() {
		boolean password = PasswordValidator.checkPasswordLength("12345678");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);
	}
	@Test
	public void testCheckPasswordLengthOut() throws Exception{
		boolean password = PasswordValidator.checkPasswordLength("1234567");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);
		
	}

	
	
	@Test
	public void testcheckdigits() {
		boolean password = PasswordValidator.checkDigits("lolololo12345");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);	
	}

	
	@Test //(expected = Exception.class)
	public void testcheckdigitsException() {
		boolean password = PasswordValidator.checkDigits("lolololo");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);	
	}

	
	
	
	@Test
	public void testcheckdigitsIn() {
		boolean password = PasswordValidator.checkDigits("lolololo12");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);	
	}

	
	@Test //(expected = Exception.class)
	public void testcheckdigitsOut() {
		boolean password = PasswordValidator.checkDigits("lolololo2");
		System.out.println(password);
		assertTrue("The password is not in correct format", password == true);	
	}


}
